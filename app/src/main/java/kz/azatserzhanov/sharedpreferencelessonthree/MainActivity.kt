package kz.azatserzhanov.sharedpreferencelessonthree

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

private const val PREF_KEY_USER_NAME = "PREF_KEY_USER_NAME"

class MainActivity : AppCompatActivity() {

    private var prefs: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prefs = PreferenceManager.getDefaultSharedPreferences(this)

        saveUserName("Azat")
        Log.d("userName", getUserName())

        saveList(mutableSetOf("кел-kel", "бар-bar", "сен-sen"))
        val loadList = prefs?.getStringSet("pref_key_list", mutableSetOf(""))
        loadList?.forEach { item ->
            Log.d("saved item:", item)
        }
    }

    private fun saveList(list: MutableSet<String>) {
        val editor = prefs?.edit()
        editor?.putStringSet("pref_key_list", list)
        editor?.apply()
    }

    private fun saveUserName(name: String) {
        val editor = prefs?.edit()
        editor?.putString(PREF_KEY_USER_NAME, name)
        editor?.apply()
    }

    private fun getUserName(): String = prefs?.getString(PREF_KEY_USER_NAME, "no") ?: ""
}
